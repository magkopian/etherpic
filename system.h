
//** system.h **//

// project-specific hardware defines used by px routines

#ifndef SYSTEM_H
#define	SYSTEM_H

// additional chip defines for C18
#ifdef __18CXX
#ifdef __18F46K80
#define _18F46K80
#endif
#ifdef __18F2550
#define _18F2550
#endif
#ifdef __18F4620
#define _18F4620
#endif
#ifdef __18F4553
#define _18F4553
#endif
#ifdef __18F26K22
#define _18F26K22
#endif
#ifdef __18F25K80
#define _18F25K80
#endif
#endif

// ET-BASE Dev Board with PIC18F46K80

#if defined(_18F46K80)

#define _XTAL_FREQ          (32000000L)

#define HAS_LCD_ET10
#define LCD_ET10_PORT       PORTD
#define LCD_ET10_LAT        LATD
#define LCD_ET10_TRIS       TRISD

#endif

// Microchip 28-Pin Demo Board with PIC16F1938

#if defined (_16F1938)

#define _XTAL_FREQ          (32000000L)

#define HAS_LCD_ET10
#define LCD_ET10_PORT       PORTB
#define LCD_ET10_LAT        LATB
#define LCD_ET10_TRIS       TRISB

#endif

// Microchip 28-Pin Demo Board with PIC18F2550

#if defined (_18F2550)

#define _XTAL_FREQ          (8000000L)

#endif

#if defined (_18F4620)

#define _XTAL_FREQ          (8000000L)

#endif

// Microchip 40-Pin Demo Board with PIC18F4553

#if defined (_18F4553)

#define _XTAL_FREQ          (8000000L)

#endif

// Microchip 28-Pin Demo Board with PIC18F26K22

#if defined (_18F26K22)

#define _XTAL_FREQ          (32000000L)

#endif

// Microchip 28-Pin Demo Board with PIC18F25K80

#if defined (_18F25K80)

#define _XTAL_FREQ          (32000000L)

#endif

#define HAS_MSSP

#if defined (_18F2550)

#define MSSP_SCK_TRIS	TRISBbits.TRISB1    // SCK Clock Out
#define MSSP_SDI_TRIS	TRISBbits.TRISB0    // SDI MISO  In
#define MSSP_SDO_TRIS	TRISCbits.TRISC7    // SDO MOSI  Out
#define MSSP_SS_TRIS	TRISAbits.TRISA5

#define MSSP_SCK_LAT	LATBbits.LATB1
#define MSSP_SDI_LAT	LATBbits.LATB0
#define MSSP_SDO_LAT	LATCbits.LATC7
#define MSSP_SS_LAT	LATAbits.LATA5

#endif

#if defined (_18F4553)

#define MSSP_SCK_TRIS	TRISBbits.TRISB1    // SCK Clock Out
#define MSSP_SDI_TRIS	TRISBbits.TRISB0    // SDI MISO  In
#define MSSP_SDO_TRIS	TRISCbits.TRISC7    // SDO MOSI  Out
#define MSSP_SS_TRIS	TRISAbits.TRISA5

#define MSSP_SCK_LAT	LATBbits.LATB1
#define MSSP_SDI_LAT	LATBbits.LATB0
#define MSSP_SDO_LAT	LATCbits.LATC7
#define MSSP_SS_LAT LATAbits.LATA5

#elif defined (_18F4620)

#define MSSP_SCK_TRIS	TRISCbits.TRISC3    // SCK Clock Out
#define MSSP_SDI_TRIS	TRISCbits.TRISC4    // SDI MISO  In
#define MSSP_SDO_TRIS	TRISCbits.TRISC5    // SDO MOSI  Out
#define MSSP_SS_TRIS	TRISAbits.TRISA5

#define MSSP_SCK_LAT	LATCbits.LATC3
#define MSSP_SDI_LAT	LATCbits.LATC4
#define MSSP_SDO_LAT	LATCbits.LATC5
#define MSSP_SS_LAT LATAbits.LATA5

#else

#define MSSP_SCK_TRIS	TRISCbits.TRISC3    // SCK Clock Out
#define MSSP_SDI_TRIS	TRISCbits.TRISC4    // SDI MISO  In
#define MSSP_SDO_TRIS	TRISCbits.TRISC5    // SDO MOSI  Out
#define MSSP_SS_TRIS	TRISAbits.TRISA5

#define MSSP_SCK_LAT	LATCbits.LATC3
#define MSSP_SDI_LAT	LATCbits.LATC4
#define MSSP_SDO_LAT	LATCbits.LATC5
#define MSSP_SS_LAT	LATAbits.LATA5

#endif

 // MSSP SPI PORTC to ENC28J60 Mini Board

#define ENC_nCS_TRIS        TRISCbits.TRISC6
#define ENC_nCS_LAT         LATCbits.LATC6
#define ENC_spiSetup        spiMsspSetup
#define ENC_spiXfer         spiMsspXfer

#endif

