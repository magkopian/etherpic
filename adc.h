/* 
 * File:   adc.h
 * Author: Manolis Agkopian
 *
 * Created on September 15, 2015, 9:35 PM
 */

#ifndef ADC_H
#define	ADC_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <plib.h>

#define adcInit() do {                                                                                   \
    CloseADC();                                                                                          \
    TRISAbits.TRISA0 = 1;                                                                                \
    OpenADC(ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_2_TAD, ADC_CH0 & ADC_INT_OFF & ADC_REF_VDD_VSS, ADC_1ANA); \
} while ( 0 )
    
#define adcClose CloseADC

inline unsigned int adcRead ( void );

#ifdef	__cplusplus
}
#endif

#endif	/* ADC_H */

