//** main.c **//

#include "pxcommon.h"
#include "pxnet.h"
#include "adc.h"
#include "aes.h"

#ifndef USE_STATIC_IP
#include "pxdhcp.h"
#endif

// For _XTAL_FREQ >= 16Mhz should be _XTAL_FREQ/2000000UL
#define T1PERSEC    (_XTAL_FREQ/2500000UL)
#define CRLF        "\r\n"


// MAC addresses starting with 80:80:80 are reserved and won't conflict with equipment manufacturers
TMacAddr gMyMAC = { 0x80, 0x80, 0x80, 0x00, 0x00, 0xA1 };

#ifndef USE_STATIC_IP
TIpAddr  gMyIP  = { 0, 0, 0, 0 };
#else
TIpAddr  gMyIP  = { 192, 168, 2, 111 };
#endif

#ifndef USE_STATIC_IP
TIpAddr  dhcp;
uint32_t leasetime;
TIpAddr  gateway;
TIpAddr  namesrv;
TIpAddr  netmask;
#else
TIpAddr  gateway = {192, 168, 2, 1};
TIpAddr  namesrv = {192, 168, 2, 1};
TIpAddr  netmask = {255, 255, 255, 0};
#endif


int8_t httpSessionStart ( TIpAddr ip_addr, uint16_t dstPort );
bool hasIpAddress ( void );
void onHttpConnect ( sock_t sock );
void onHttpResponse ( uint16_t status, char *data );
void initialize ( void );


void main ( void ) {
    uint16_t seconds = 0;
    uint8_t i, count = 0;

    initialize();

    // Use noise from a floating ADC input to initialize rand()
    adcInit();
    srand(adcRead());
    adcClose();
    
    // Initialize and start networking
    netInit();
    netEnable();

    // Enable interrupts
    INTCONbits.GIE = 1;

    // The IP of the remote HTTP server
    TIpAddr  ip_addr  = { 192, 168, 2, 180 };
    
    int8_t requestStatus = 0;
    
    for ( ;; ) {
        
        // Every time TMR1 overflows
        if ( PIR1bits.TMR1IF ) {
            
            PIR1bits.TMR1IF = 0;
            count++;
            
            // If the HTTP request failed try again without waiting for 3 whole seconds
            if ( hasIpAddress() && requestStatus == -1 ) {
                requestStatus = httpSessionStart(ip_addr, 80);
            }
            
            // Approximately every 1s (1.048576s)
            if ( count >= T1PERSEC ) {

                count = 0;
                seconds++;
                if ( seconds > 999 ) {
                    seconds = 0;
                }

                netTimer( seconds );
                
                // Every three seconds make an HTTP request to the web server
                if ( hasIpAddress() && seconds % 3 == 0 ) {
                    requestStatus = httpSessionStart(ip_addr, 80);
                }
                
                #ifndef USE_STATIC_IP
                // Every 10 seconds to get an IP until the DHCP servers responds
                if ( !hasIpAddress() && (seconds == 1 || seconds % 10 == 0) ) {
                    dhcpDiscover();
                }
                #endif

            }
            
        }
        
        netIdle();
        
    }
    
}

void initialize ( void ) {
    uint8_t i;

#if defined(_16F1938)
    OSCCONbits.IRCF = 0b1110; // 8MHz
    OSCCONbits.SPLLEN = 1;    // 4xPLL 32MHz
    ANSELA = 0x00;
    ANSELB = 0x00;
#endif

#if defined(_18F46K80) || defined(_18F25K80)
    OSCCONbits.IRCF = 0b110; // 8MHz
    OSCTUNEbits.PLLEN = 1;   // 4xPLL 32MHz
    ANCON0 = 0x00;
    ANCON1 = 0x00;
#endif
    
#if defined(_18F2550)
    OSCCONbits.IRCF = 0b111; // 8MHz
    ADCON1bits.PCFG = 0b1110; // All I/O pins are digital except AN0 (RA0)
#endif

#if defined(_18F4553)
    OSCCONbits.IRCF = 0b111; // TIMER 1 is 8MHz (INTOSC drives clock directly)
    ADCON1bits.PCFG = 0b1110; // All I/O pins are digital except AN0 (RA0)
#endif
    
#if defined(_18F26K22)
    OSCCONbits.IRCF = 0b110; // 8MHz
    OSCTUNEbits.PLLEN = 1;   // 4xPLL 32MHz
    ANSELA = 0x00;
    ANSELB = 0x00;
    ANSELC = 0x00;
#endif

    // wait 1 second for PICkit3 to use MCLR/PGC/PGD
    for ( i = 0; i < 100; i++ ) __delay_ms( 10 );

    T1CONbits.TMR1CS = 0; // Internal clock (FOSC/4)
    T1CONbits.T1CKPS = 0b11; // 1:8 Prescale value
    T1CONbits.TMR1ON = 1;
    
    // Timer 1 is going to overflow every 2^16 / ( FOSC / 4 / 8 ) = 262.144ms fpr FOSC = 8Mhz
    
}

/* // Currently not used anywhere

static int8_t strncasecmpram( const char *s1, const char *s2, uint8_t n )
{
    int8_t i;

    for ( i = 0; i < n; i++ ) {
        int8_t c = toupper( *s1++ ) - toupper( *s2++ );
        if ( c ) return c;
    }
    return 0;
}


#ifdef __18CXX
static int8_t strncasecmppgm( const char *s1, const far rom char *s2, uint8_t n ) {
    
    int8_t i;

    for ( i = 0; i < n; i++ ) {
        int8_t c = toupper( *s1++ ) - toupper( *s2++ );
        if ( c ) return c;
    }
    
    return 0;
    
}
#else
#define strncasecmppgm strncasecmpram
#endif

*/

static void soutram ( sock_t sock, const char* s ) {
    netPutAppend( sock, s, strlen( s ) );
}


#ifdef __18CXX
static void soutpgm ( sock_t sock, const far rom char* s ) {
    
    uint8_t buffer[8];
    uint8_t count = 0;
    
    while ( *s ) {
        buffer[count++] = *s;
        if ( count == sizeof( buffer ) ) {
            netPutAppend( sock, buffer, count );
            count = 0;
        }
        s++;
    }
    
    if ( count ) {
        netPutAppend( sock, buffer, count );
    }
    
}
#else
#define soutpgm soutram
#endif

int8_t httpSessionStart ( TIpAddr ip_addr, uint16_t dstPort ) {
    
    sock_t sock;
    
    if ( sock = tcpEstablishConnection(ip_addr, dstPort, 0, TCP_PROTO_HTTP) == -1 ) { 
        return -1;
    }
    
    return 0;
    
}

bool hasIpAddress ( void ) {
    
    return !(gMyIP[0] == 0 && gMyIP[1] == 0 && gMyIP[2] == 0 && gMyIP[3] == 0);
    
}

////////////////////////////////////////////////////////////////////////////////
//
// Callbacks from pxnet TCP sockets
//
void onHttpResponse ( uint16_t status, char *data ) {
    
    if ( status == 200 ) {
        if ( strcmp(data, "Decrypted: Hello World!") == 0 ) {
            //...
        }
    }
    else {
        //...
    }
    
}

void onHttpConnect ( sock_t sock ) {
    
    uint8_t i, encrypted_text[16];
    char encrypted_text_hex[33];

    // 128bit key
    uint8_t key[16] = { (uint8_t) 0x2b, (uint8_t) 0x7e, (uint8_t) 0x15, (uint8_t) 0x16, (uint8_t) 0x28, (uint8_t) 0xae, (uint8_t) 0xd2, (uint8_t) 0xa6, (uint8_t) 0xab, (uint8_t) 0xf7, (uint8_t) 0x15, (uint8_t) 0x88, (uint8_t) 0x09, (uint8_t) 0xcf, (uint8_t) 0x4f, (uint8_t) 0x3c };

    // 128bit text
    // 48 65 6c 6c 6f 20 57 6f 72 6c 64 21
    uint8_t plain_text[16] = "Hello World!\0\0\0\0";
    memset(encrypted_text, 0, 16);

    // Send HTTP Request
    netPutBegin( sock );

    soutpgm( sock, "POST /aes.php HTTP/1.0" CRLF );
    soutpgm( sock, "User-Agent: PIC18LF4553" CRLF );
    soutpgm( sock, "Host: 192.168.2.180" CRLF );
    soutpgm( sock, "Accept: */*" CRLF );
    soutpgm( sock, "Content-Type: application/x-www-form-urlencoded" CRLF );
    soutpgm( sock, "Content-Length: 37" CRLF );
    soutpgm( sock, CRLF );

    soutpgm( sock, "data=");
    AES128_ECB_encrypt(plain_text, key, encrypted_text);

    for (i = 0; i < 16; ++i) {
        sprintf(encrypted_text_hex + i*2, "%.2x", encrypted_text[i]);
    }
    encrypted_text_hex[32] = '\0';

    soutpgm(sock, encrypted_text_hex);

    netPutFinalize( sock );
        
}
