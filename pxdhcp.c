//** pxdhcp.c **//

#include "pxcommon.h"

#ifndef USE_STATIC_IP

#include "pxmac.h"
#include "pxnet.h"
#include "pxdhcp.h"

#if defined(__XC8) || defined(__PICC__)
#define FIX_XC8_BUGS
#endif

static uint32_t dhcpTranId = 0;
static uint8_t dhcpState = 0;

TIpAddr  offeredIpAddr;

static int8_t sendDhcpPacket ( uint8_t type ) {
    
    /*TMacAddr en_remMac;
    
    if ( arpTableLookup(ip_remIp, (TMacAddr *) &en_remMac) == -1 ) {
        sendARPRequest( ip_remIp );
        return -1;
    }
    */
    
    TIpAddr ip_locIp = { 0, 0, 0, 0 };
    TIpAddr ip_remIp = { 255, 255, 255, 255 };
    TMacAddr en_remMac = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    USocket srec;
    
    if ( type == DHCPDISCOVER ) {
        do {
            dhcpTranId = rand();
            dhcpTranId = (dhcpTranId << 16) | rand();
        } while ( dhcpTranId == 0 );
    }
    else if ( type != DHCPREQUEST ) {
        return -1;
    }
    
    srec.locPort = 68;
    srec.remPort = 67;
    memcpy( srec.remMac, en_remMac, kMACLEN );
    memcpy( srec.remIp, ip_remIp, kIPLEN );
    memcpy( srec.locIp, ip_locIp, kIPLEN );

    udpTxInit( &srec );
    
    memset( buff, 0, 20 );
    buff[0] = 0x01; // Message type: Boot Request (1)
    buff[1] = 0x01; // Hardware type: Ethernet (0x01)
    buff[2] = 0x06; // Hardware address length: 6
    // Hops: 0
    setu32( buff + 4, htonl( dhcpTranId ) );
    // Seconds elapsed: 0
    // Bootp flags: 0x0000 (Unicast)
    // Client IP address: 0.0.0.0 (0.0.0.0)
    // Your (client) IP address: 0.0.0.0 (0.0.0.0)
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Next server IP address: 0.0.0.0 (0.0.0.0)
    // Relay agent IP address: 0.0.0.0 (0.0.0.0)
    buff[8] = gMyMAC[0]; // Client MAC address: 80:80:80:00:00:a1 (80:80:80:00:00:a1)
    buff[9] = gMyMAC[1];
    buff[10] = gMyMAC[2];
    buff[11] = gMyMAC[3];
    buff[12] = gMyMAC[4];
    buff[13] = gMyMAC[5];
    // Client hardware address padding: 00000000000000000000 +6 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Client hardware address padding: 00000000000000000000 +4 zeros
    // Server host name not given (64 bytes total) +16 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Server host name not given (64 bytes total) +20 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Server host name not given (64 bytes total) +20 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Server host name not given (64 bytes total) +8 zeros
    // Boot file name not given (128 bytes total) +12 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Boot file name not given (128 bytes total) +20 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Boot file name not given (128 bytes total) +20 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Boot file name not given (128 bytes total) +20 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Boot file name not given (128 bytes total) +20 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Boot file name not given (128 bytes total) +20 zeros
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Boot file name not given (128 bytes total) +16 zeros
    buff[16] = 0x63; // Magic cookie: DHCP
    buff[17] = 0x82;
    buff[18] = 0x53;
    buff[19] = 0x63;
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    buff[0] = 0x35; // Option: (53) DHCP Message Type (Discover)
    buff[1] = 0x01; // Length: 1
    buff[2] = type; // DHCP: Discover (1) or Request (3)
    buff[3] = 0x37; // Option: (55) Parameter Request List 
    buff[4] = 0x12; // Length: 18
    buff[5] = 0x01; // Parameter Request List Item: (1) Subnet Mask
    buff[6] = 0x1C; // Parameter Request List Item: (28) Broadcast Address
    buff[7] = 0x02; // Parameter Request List Item: (2) Time Offset
    buff[8] = 0x03; // Parameter Request List Item: (3) Router
    buff[9] = 0x0F; // Parameter Request List Item: (15) Domain Name
    buff[10] = 0x06; // Parameter Request List Item: (6) Domain Name Server
    buff[11] = 0x77; // Parameter Request List Item: (119) Domain Search
    buff[12] = 0x0C; // Parameter Request List Item: (12) Host Name
    buff[13] = 0x2C; // Parameter Request List Item: (44) NetBIOS over TCP/IP Name Server
    buff[14] = 0x2F; // Parameter Request List Item: (47) NetBIOS over TCP/IP Scope
    buff[15] = 0x1A; // Parameter Request List Item: (26) Interface MTU
    buff[16] = 0x79; // Parameter Request List Item: (121) Classless Static Route
    buff[17] = 0x2A; // Parameter Request List Item: (42) Network Time Protocol Servers
    buff[18] = 0x79; // Parameter Request List Item: (121) Classless Static Route
    buff[19] = 0xF9; // Parameter Request List Item: (249) Private/Classless Static Route (Microsoft)
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    buff[0] = 0x21; // Parameter Request List Item: (33) Static Route
    buff[1] = 0xFC; // Parameter Request List Item: (252) Private/Proxy autodiscovery
    buff[2] = 0x2A; // Parameter Request List Item: (42) Network Time Protocol Servers
    if ( type == DHCPDISCOVER ) {
        buff[3] = 0xFF; // Option: (255) End
    }
    else if ( type == DHCPREQUEST ) {
        buff[3] = 0x36; // Option: (54) DHCP Server Identifier
        buff[4] = 0x04; // Length: 4
        buff[5] = dhcp[0]; // DHCP Server Identifier: <ip-of-dhcp-server>
        buff[6] = dhcp[1];
        buff[7] = dhcp[2];
        buff[8] = dhcp[3];
        buff[9] = 0x32; // Option: (50) Requested IP Address
        buff[10] = 0x04; // Length: 4
        buff[11] = offeredIpAddr[0]; // Requested IP Address: <offered-ip-address>
        buff[12] = offeredIpAddr[1];
        buff[13] = offeredIpAddr[2];
        buff[14] = offeredIpAddr[3];
        buff[15] = 0xFF; // Option: (255) End
    }
    // Padding
    udpTxAppend( buff, 20 );
    
    memset( buff, 0, 20 );
    // Padding
    udpTxAppend( buff, 20 );
    
    return udpTxFinalize( &srec );

}

void handleDHCP ( TPacket* pkt ) {
    
    uint8_t msgType, dhcpMsgType, hwType, hwAddrLen, hops;
    uint32_t tranId;
    uint16_t secs, flags;
    TIpAddr clientIpAddr, yourIpAddr, nextSrvIpAddr, relayAgentIpAddr;
    TMacAddr clientMacAddr;
    uint32_t magicCookie;

    if ( dhcpState == DHCP_IDLE ) {
        return;
    }
    
    // Read first 40 bytes from DHCP header
    memset( buff, 0, 100 );
    macRxRead( buff, 100 );
    
    // If the packet is not a boot reply drop it
    msgType = buff[0];
    if ( msgType != 0x02 ) {
        return;
    }
    
    hwType = buff[1];
    hwAddrLen = buff[2];
    hops = buff[3];
    
    // If the transaction ID does not much drop the packet
    tranId = ntohl( getu32( buff + 4 ) );
    if ( tranId != dhcpTranId || dhcpTranId == 0 ) {
        return;
    }
    
    secs = ntohs( getu16( buff + 8 ) );
    flags = ntohs( getu16( buff + 10 ) );
    
    clientIpAddr[0] = buff[12];
    clientIpAddr[1] = buff[13];
    clientIpAddr[2] = buff[14];
    clientIpAddr[3] = buff[15];
    
    yourIpAddr[0] = buff[16];
    yourIpAddr[1] = buff[17];
    yourIpAddr[2] = buff[18];
    yourIpAddr[3] = buff[19];
    
    nextSrvIpAddr[0] = buff[20];
    nextSrvIpAddr[1] = buff[21];
    nextSrvIpAddr[2] = buff[22];
    nextSrvIpAddr[3] = buff[23];
    
    relayAgentIpAddr[0] = buff[24];
    relayAgentIpAddr[1] = buff[25];
    relayAgentIpAddr[2] = buff[26];
    relayAgentIpAddr[3] = buff[27];
    
    clientMacAddr[0] = buff[28];
    clientMacAddr[1] = buff[29];
    clientMacAddr[2] = buff[30];
    clientMacAddr[3] = buff[31];
    clientMacAddr[4] = buff[32];
    clientMacAddr[5] = buff[33];
    
    // If the MAC address does not belong to the device drop the packet (this should never happen)
    if ( memcmp( clientMacAddr, gMyMAC, kMACLEN ) != 0 ) {
        return;
    }
    
    // client hardware padding 10 bytes 40 + 4 = 44
    
    // Server host name not given ( total 64 Bytes ) + 56
    
    // Read next 100 bytes
    memset( buff, 0, 100 );
    macRxRead( buff, 100 );
    
    // Server host name not given ( total 64 Bytes ) + 8
    
    // Boot file name not given + 92 Bytes ( total 128 Bytes )
    
    // Read next 100 bytes
    memset( buff, 0, 40 );
    macRxRead( buff, 40 );
    
    // Boot file name not given + 36 Bytes ( total 128 Bytes )
    
    // If the value of the magic cookie does not indicate a DHCP packet drop it
    magicCookie = ntohl( getu32( buff + 36 ) );
    if ( magicCookie != 0x63825363 ) {
        return;
    }
    
    // At this point 240 Bytes have been read from the DHCP packet, 60 more to go
    
    // Read next 60 bytes
    memset( buff, 0, 60 );
    macRxRead( buff, 60 );
    
    for ( uint8_t j = 0; j < 60; ++j ) {

        // Read until the END option is found or we reach the end
        if ( buff[j] == 0xFF ) {
            break;
        }
        else if ( buff[j] == 0x35 ) { // Option: (53) DHCP Message Type
            if ( j + 3 > 59 ) {
                break;
            }
            
            j += 2;

            dhcpMsgType = buff[j];
            
            if ( dhcpMsgType == DHCPOFFER && dhcpState == DHCP_WAIT_FOR_ACK ) { // Accept only the first offer
                return;
            }
            else if ( dhcpMsgType == DHCPACK && dhcpState != DHCP_WAIT_FOR_ACK ) { // If DHCPACK is received without expecting it drop it
                return;
            }
            else if ( dhcpMsgType == DHCPNAK && dhcpState != DHCP_WAIT_FOR_ACK ) { // If DHCPNAK is received without expecting it ignore it
                return;
            }
            else if ( dhcpMsgType == DHCPNAK && dhcpState == DHCP_WAIT_FOR_ACK ) { // If DHCPACK is received while waiting for DHCPACK reset
                offeredIpAddr[0] = offeredIpAddr[1] = offeredIpAddr[2] = offeredIpAddr[3] = 0;
                dhcpTranId = 0;
                dhcpState = DHCP_IDLE;
                return;
            }
            else if ( dhcpMsgType != DHCPOFFER && dhcpMsgType != DHCPACK && dhcpMsgType != DHCPNAK ) { // If packet is not DHCPOFFER, DHCPACK or DHCPNAK drop it
                return;
            }

            continue;
        }
        else if ( buff[j] == 0x36 ) { // Option: (54) DHCP Server Identifier
            if ( j + 6 > 59 ) {
                break;
            }
            
            j += 2;

            dhcp[0] = buff[j++];
            dhcp[1] = buff[j++];
            dhcp[2] = buff[j++];
            dhcp[3] = buff[j];

            continue;
        }
        else if ( buff[j] == 0x33 ) { // Option: (51) IP Address Lease Time
            if ( j + 6 > 59 ) {
                break;
            }
            
            j += 2;

            leasetime = ntohl( getu32( buff + j ) );
            j += 3;

            continue;
        }
        else if ( buff[j] == 0x01 ) { // Option: (1) Subnet Mask
            if ( j + 6 > 59 ) {
                break;
            }
            
            j += 2;

            netmask[0] = buff[j++];
            netmask[1] = buff[j++];
            netmask[2] = buff[j++];
            netmask[3] = buff[j];

            continue;
        }
        else if ( buff[j] == 0x03 ) { // Option: (3) Router
            if ( j + 6 > 59 ) {
                break;
            }
            
            j += 2;

            gateway[0] = buff[j++];
            gateway[1] = buff[j++];
            gateway[2] = buff[j++];
            gateway[3] = buff[j];

            continue;
        }
        else if ( buff[j] == 0x06 ) { // Option: (6) Domain Name Server
            if ( j + 6 > 59 ) {
                break;
            }
            
            j += 2;

            namesrv[0] = buff[j++];
            namesrv[1] = buff[j++];
            namesrv[2] = buff[j++];
            namesrv[3] = buff[j];

            continue;
        }
        else { // If the option is unknown we just increase the pointer the same amount the length of the option is
            if ( j + 1 > 59 ) {
                break;
            }
            
            j += buff[++j];
            
            continue;
        }

    }
    
    // If this is an IP offer
    if ( dhcpMsgType == DHCPOFFER ) {
        
        offeredIpAddr[0] = yourIpAddr[0];
        offeredIpAddr[1] = yourIpAddr[1];
        offeredIpAddr[2] = yourIpAddr[2];
        offeredIpAddr[3] = yourIpAddr[3];
        
        dhcpRequest();
    }
    else if ( dhcpMsgType == DHCPACK ) {
        gMyIP[0] = offeredIpAddr[0];
        gMyIP[1] = offeredIpAddr[1];
        gMyIP[2] = offeredIpAddr[2];
        gMyIP[3] = offeredIpAddr[3];
        
        offeredIpAddr[0] = offeredIpAddr[1] = offeredIpAddr[2] = offeredIpAddr[3] = 0;
        dhcpTranId = 0;
        dhcpState = DHCP_IDLE;
    }
    
}

int8_t dhcpRequest ( void ) {
    
    int8_t status = 0;
    
    status = sendDhcpPacket(DHCPREQUEST);
    
    if ( status > 0 ) {
        dhcpState = DHCP_WAIT_FOR_ACK;
    }
    
    return status;

}

int8_t dhcpDiscover ( void ) {
    
    int8_t status = 0;
    
    status = sendDhcpPacket(DHCPDISCOVER);
    
    if ( status > 0 ) {
        dhcpState = DHCP_WAIT_FOR_OFFER;
    }
    
    return status;
    
}

#endif
