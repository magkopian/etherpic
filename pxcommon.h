//** pxcommon.h **//

#ifndef PXCOMMON_H
#define	PXCOMMON_H

//#define USE_STATIC_IP

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#if defined(__XC)
    #include <stdint.h>
    #include <stdbool.h>
    #include <xc.h>
#elif defined(__18CXX)
    #include <p18cxxx.h>
#elif defined(__PICC__)
    #include <stdint.h>
    #include <stdbool.h>
    #include <htc.h>
#endif

#ifdef __18CXX

    #include <delays.h>

    typedef unsigned char   uint8_t;
    typedef unsigned short  uint16_t;
    typedef unsigned long   uint32_t;

    typedef char    int8_t;
    typedef short   int16_t;
    typedef long    int32_t;

    typedef unsigned char   bool;

    #define true 1
    #define false 0

    extern void _delay( unsigned long cy );

    #define __delay_us(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000000.0)))
    #define __delay_ms(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000.0)))

#endif


#include "system.h"

#define MASKIN(reg,mask,x) do{reg^=(reg^(x))&(mask);}while(0)

void uintoa( char *out, uint16_t v );

extern uint8_t	buff[];
extern uint8_t	data_buff[];

#ifndef __18CXX
#define strcpypgm2ram strcpy
#define strcatpgm2ram strcat
#define strlenpgm strlen
#endif

#ifndef Nop
#define Nop NOP
#endif

#endif
