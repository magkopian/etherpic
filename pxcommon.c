//** pxcommon.c **//

#include "pxcommon.h"

uint8_t	buff[100];
uint8_t	data_buff[800];

void uintoa( char *out, uint16_t v )
{
    uint8_t work[5] = { 0 };
    int count = 0;

    do {
       work[count++] = v % 10;
      v /= 10;
    } while ( v );

    while ( count > 0 )
      *out++ = '0' + work[--count];
    *out = 0;
}


#ifdef __18CXX

void _delay( unsigned long cy )
{
    if ( cy == 1 ) return;
    
    if      ( cy > 200000UL ) Delay10KTCYx( cy / 10000 );
    else if ( cy > 20000    ) Delay1KTCYx ( cy / 1000 );
    else if ( cy > 2000     ) Delay100TCYx( cy / 100 );
    else Delay10TCYx( cy / 10 );
}

#endif
