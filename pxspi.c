
//** pxspi.c **//

#include "pxcommon.h"
#include "pxspi.h"


////////////////////////////////////////////////////////////////////////////////
//
// SPI using MCU MSSP
//

#ifdef HAS_MSSP

void spiMsspSetup( uint8_t flags )
{
    if ( flags & kSPI_MASTER ) {
        uint8_t sspm;

        // tri-state the SPI pins
        MSSP_SCK_TRIS = 0; // SPI clock output
        MSSP_SDI_TRIS = 1; // SPI data input
        MSSP_SDO_TRIS = 0; // SPI data output

        // 16F1938 header file doesn't have SSPM defined
        // so set each SSPMx bit

        sspm = 0b0001; // Fosc/16

        switch ( flags & kSPI_CLK_MASK ) {
        case kSPI_CLK_DIV_4:
            sspm = 0b0000; // Fosc / 4
            break;
#if defined(_18F46K80)
        case kSPI_CLK_DIV_8:
            sspm = 0b1010; // Fosc / 8
            break;
#endif
        case kSPI_CLK_DIV_64:
            sspm = 0b0010; // Fosc / 64
            break;
        }
        SSPCON1bits.SSPM0 = ( sspm & 0b0001 ) != 0;
        SSPCON1bits.SSPM1 = ( sspm & 0b0010 ) != 0;
        SSPCON1bits.SSPM2 = ( sspm & 0b0100 ) != 0;
        SSPCON1bits.SSPM3 = ( sspm & 0b1000 ) != 0;
        
        if ( flags & kSPI_IDLE_HIGH )
            SSPCON1bits.CKP     = 1; // clock idle high
        else
            SSPCON1bits.CKP     = 0; // clock idle low

        if ( flags & kSPI_TX_ATOI )
            SSPSTATbits.CKE     = 1; // tx on clock active to idle
        else
            SSPSTATbits.CKE     = 0; // tx on clock idle to active

        if ( flags & kSPI_SAMP_END )
            SSPSTATbits.SMP = 1;
        else
            SSPSTATbits.SMP = 0;

        SSPCON1bits.SSPEN   = 1;
    } else {
        SSPCON1bits.SSPEN   = 0;
    }
}


int spiMsspXfer( uint8_t out, uint8_t* in )
{
    SSPCON1bits.WCOL = 0;
    SSPSTATbits.BF = 0;
    PIR1bits.SSPIF = 0;

    // start data transfer
    SSPBUF = out;

    // wait for completion
    while ( ! SSPSTATbits.BF )
        ;

    // read incoming data
    *in = SSPBUF;

    return 0;
}

#endif


////////////////////////////////////////////////////////////////////////////////
//
// SPI using software bit banging
//

#ifdef HAS_SW_SPI


static uint8_t gSpiFlags    = 0x00;
static uint8_t gSpiClkIdle  = 0;

void spiSwSetup( uint8_t flags )
{
    if ( flags & kSPI_MASTER ) {
        // set clock and data idle
        gSpiClkIdle = (flags & kSPI_IDLE_HIGH) ? 1 : 0;

        SW_SPI_SCK_LAT = gSpiClkIdle;
        SW_SPI_SDO_LAT = 0;

        // tri-state the SPI pins
        SW_SPI_SCK_TRIS = 0; // SPI clock output
        SW_SPI_SDI_TRIS = 1; // SPI data input
        SW_SPI_SDO_TRIS = 0; // SPI data output

        gSpiFlags = flags;
    }
}


static void spiSwDelay()
{
    // delay for 1/2 bit
    switch ( gSpiFlags & kSPI_CLK_MASK ) {
    case kSPI_CLK_DIV_4:
        _delay( 2 ); // Fosc / 4
        break;
    case kSPI_CLK_DIV_8:
        _delay( 4 ); // Fosc / 8
        break;
    case kSPI_CLK_DIV_16:
        _delay( 8 ); // Fosc / 16
        break;
    default:
    case kSPI_CLK_DIV_64:
        _delay( 32 ); // Fosc / 64
        break;
    }
}


int spiSwXfer( uint8_t out, uint8_t* in )
{
    bool atEnd = ( gSpiFlags & kSPI_SAMP_END ) != 0;
    // shift out msb, shift in lsb
    if ( gSpiFlags & kSPI_TX_ATOI ) {
        // tx on trailing edge, sample on leading edge CKE=1
        for ( uint8_t i = 0; i < 8; i++ ) {
            SW_SPI_SDO_LAT = ( out & 0x80 ) ? 1 : 0;
            out <<= 1;
            spiSwDelay();
            SW_SPI_SCK_LAT = ! gSpiClkIdle;
            if ( atEnd ) spiSwDelay();
            if ( SW_SPI_SDI_PORT ) out |= 0x01;
            if ( ! atEnd ) spiSwDelay();
            SW_SPI_SCK_LAT = gSpiClkIdle;
        }
    } else {
        // tx on leading edge, sample on trailing edge CKE=0
        spiSwDelay();
        for ( uint8_t i = 0; i < 8; i++ ) {
            SW_SPI_SDO_LAT = ( out & 0x80 ) ? 1 : 0;
            out <<= 1;
            SW_SPI_SCK_LAT = ! gSpiClkIdle;
            spiSwDelay();
            SW_SPI_SCK_LAT = gSpiClkIdle;
            if ( atEnd ) spiSwDelay();
            if ( SW_SPI_SDI_PORT ) out |= 0x01;
            if ( ! atEnd ) spiSwDelay();
        }
    }
    *in = out;

    return 0;
}

#endif
