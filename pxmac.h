//** pxmac.h **//

#ifndef PXMAC_H
#define PXMAC_H

#define kMACLEN     (6)
#define kTXBUFFERS  (4)

typedef uint8_t TMacAddr[kMACLEN];

extern TMacAddr gMyMAC;

void     macInit( void );
void     macEnable( void );
uint16_t macRxGetBufferFree( void );

void     macTxBegin( void );
void     macTxWrite( const uint8_t *src, uint16_t bytes );
void     macTxSetPtr( uint16_t offset );
void     macTxSetLen( uint16_t bytes );
void     macTxSend( void );

uint8_t  macRxPoll( void );

void     macRxBegin( void );
void     macRxRead( uint8_t *dst, uint16_t bytes );
uint16_t macRxGetLen( void );
void     macRxEnd( void );

#endif
