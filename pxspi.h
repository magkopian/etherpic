
//** pxspi.h **//

#ifndef PXSPI_H
#define PXSPI_H

#define kSPI_MASTER     (0x01)
#define kSPI_DISABLE    (0x00)
#define kSPI_IDLE_LOW   (0x00)
#define kSPI_IDLE_HIGH  (0x02)
#define kSPI_CLK_DIV_4  (0x00)
#define kSPI_CLK_DIV_8  (0x04)
#define kSPI_CLK_DIV_16 (0x08)
#define kSPI_CLK_DIV_64 (0x0C)
#define kSPI_CLK_MASK   (0x0C)
#define kSPI_SAMP_MID   (0x00)
#define kSPI_SAMP_END   (0x10)
#define kSPI_TX_ITOA    (0x00)
#define kSPI_TX_ATOI    (0x20)

void spiMsspSetup( uint8_t flags );
int  spiMsspXfer( uint8_t out, uint8_t* in );

void spiSwSetup( uint8_t flags );
int  spiSwXfer( uint8_t out, uint8_t* in );

#endif

