//** pxdhcp.h **//

#ifndef USE_STATIC_IP
#ifndef PXDHCP_H
#define PXDHCP_H

#define DHCP_IDLE 0x00
#define DHCP_WAIT_FOR_OFFER 0x01
#define DHCP_WAIT_FOR_ACK 0x02

#define DHCPDISCOVER 0x01
#define DHCPOFFER 0x02
#define DHCPREQUEST 0x03
#define DHCPDECLINE 0x04
#define DHCPACK 0x05
#define DHCPNAK 0x06
#define DHCPRELEASE 0x07
#define DHCPINFORM 0x08

extern TIpAddr   gMyIP;

extern TIpAddr  dhcp;
extern TIpAddr  offeredIpAddr;
extern TIpAddr  gateway;
extern TIpAddr  namesrv;
extern TIpAddr  netmask;
extern uint32_t leasetime;

int8_t dhcpDiscover ( void );
int8_t dhcpRequest ( void );

void handleDHCP( TPacket* pkt );

#endif
#endif
