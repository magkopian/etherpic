//** pxnet.h **//

#ifndef PXNET_H
#define PXNET_H

#include "pxmac.h"

#define kMAXSOCKETS     (8)
#define kMAXARPRECORDS  (8)
#define kIPLEN          (4)
#define kTIME_WAIT_SECS (15)
#define kINACTIVE_SECS  (30)

#define MIN_EPHEMERAL_SOCKET	32768
#define MAX_EPHEMERAL_SOCKET	61000

#define ETYPE_IPV4	(0x0800)
#define ETYPE_ARP	(0x0806)
#define ETYPE_802_1Q	(0x8100)

#define IPV4_PROTO_ICMP (1)
#define IPV4_PROTO_TCP 	(6)
#define IPV4_PROTO_UDP 	(17)

#define DEFAULT_TTL	(64)

#define ETH_HEADER_LEN	(14)
#define IPV4_HEADER_LEN	(20)

#define ETH_MTU         (1500)

#define DEST_OTHER	(0)
#define DEST_UNICAST	(1)
#define DEST_MULTICAST	(2)
#define DEST_BROADCAST	(3)

#define TCP_FLAG_FIN	(0x01)
#define TCP_FLAG_SYN	(0x02)
#define TCP_FLAG_RST	(0x04)
#define TCP_FLAG_PSH	(0x08)
#define TCP_FLAG_ACK	(0x10)

#define TCP_PROTO_HTTP  (1)

#define CBF_CONNECT     (0x01)
#define CBF_DISCONNECT  (0x02)

typedef enum {
    CLOSED,
    SYN_RECEIVED,
    SYN_SENT,
    ESTABLISHED,
    DATA_SENT_WAIT,
    FIN_WAIT_1,
    FIN_WAIT_2,
    CLOSE_WAIT,
    CLOSING,
    LAST_ACK,
    TIME_WAIT
} TcpState;

typedef uint8_t  TIpAddr [kIPLEN];
typedef int8_t   sock_t;

typedef struct {
    TcpState	state;
    uint16_t	locPort;
    uint16_t	remPort;
    TMacAddr	remMac;
    TIpAddr     remIp;
    uint32_t	remSeqNo;
    uint32_t	locSeqNo;
    uint8_t     timer;
    uint8_t     callbacks;
    uint8_t     protocol;
} TSocket;

typedef struct {
    uint16_t	locPort;
    uint16_t	remPort;
    TMacAddr	remMac;
    TIpAddr     remIp;
    TIpAddr     locIp;
} USocket;

typedef struct {
    TIpAddr     ip_addr;
    TMacAddr    mac_addr;
} ArpRecord;

typedef struct {
    // from ethernet frame
    TMacAddr	en_dstMac;
    TMacAddr	en_srcMac;
    uint16_t	en_etype;
    uint8_t     en_dest;
    // from IPv4 header
    TIpAddr     ip_dstIp;
    TIpAddr     ip_srcIp;
    uint8_t     ip_ttl;
    uint16_t	ip_len;
} TPacket;

extern TIpAddr   gMyIP;

void udpTxInit ( USocket *srec );
void udpTxAppend ( const void *dataPtr, uint16_t dataLen );
uint16_t udpTxFinalize ( USocket *srec );
void handleUDP ( TPacket* pkt );

void setu16 ( uint8_t* dst, uint16_t u16 );
void setu32 ( uint8_t *dst, uint32_t u32 );
uint16_t getu16 ( uint8_t *src );
uint32_t getu32 ( uint8_t *src );

uint16_t htons ( uint16_t n );
uint32_t htonl ( uint32_t n );
#define ntohs htons
#define ntohl htonl

void netInit ( void );
void netEnable ( void );
void netIdle ( void );
void netTimer ( uint32_t seconds );

void netRead ( sock_t sock, uint8_t* dst, uint16_t bytes );
void netWrite ( sock_t sock, const uint8_t *src, uint16_t bytes );
void netClose ( sock_t sock );

void netPutBegin ( sock_t sock );
void netPutAppend ( sock_t sock, const uint8_t *src, uint16_t bytes );
void netPutFinalize ( sock_t sock );

extern void onHttpConnect ( sock_t sock );
extern void onHttpResponse ( uint16_t status, char *data );

int8_t tcpEstablishConnection ( TIpAddr ip_remIp, uint16_t dstPort, uint16_t srcPort, uint8_t protocol );

void netGetAddr ( sock_t sock, uint16_t* port, uint8_t* ip, uint8_t *mac, uint8_t* state );

int8_t arpTableLookup ( TIpAddr ip_addr, TMacAddr *mac_addr );

#endif
