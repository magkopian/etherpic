/* 
 * File:   adc.c
 * Author: Manolis Agkopian
 *
 * Created on September 15, 2015, 9:35 PM
 */

#include "pxcommon.h"
#include <plib.h>

// TODO: Add support for 16F1938, 18F46K80, 18F25K80 and 18F26K22

inline unsigned int adcRead ( void ) {
    
    ConvertADC();
    while ( BusyADC() );
    return ReadADC();
    
}
