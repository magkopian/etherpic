
//** pxmac.c **//

#include "pxcommon.h"
#include "pxmac.h"
#include "pxenc.h"

static uint8_t  gRxHeader[6] = { 0 };
static uint16_t gRxLength    = 0;


void    macInit()
{
    encSetup( 0 );
    
    encBankSelect( 3 );
    encWriteRegister( R_MAADR1, gMyMAC[0] );
    encWriteRegister( R_MAADR2, gMyMAC[1] );
    encWriteRegister( R_MAADR3, gMyMAC[2] );
    encWriteRegister( R_MAADR4, gMyMAC[3] );
    encWriteRegister( R_MAADR5, gMyMAC[4] );
    encWriteRegister( R_MAADR6, gMyMAC[5] );
}


void    macEnable()
{
    encEnable();
}


uint16_t macRxGetBufferFree()
{
    return encGetRxBufferFree();
}


void    macTxBegin()
{
    uint8_t ctb;

    // wait for buffer empty
    uint8_t u = 0;
    do {
        encReadRegister( R_ECON1, &u );
    } while ( u & B_TXRTS );

    // set Buffer Write Pointer
    encBankSelect( 0 );
    encWriteRegister( R_EWRPTL, 0x00 );
    encWriteRegister( R_EWRPTH, 0x19 );

    // control byte
    ctb = 0x00;
    encWriteBuffer( &ctb, 1 );
}


void    macTxWrite( const uint8_t *src, uint16_t bytes )
{
    encWriteBuffer( src, bytes );
}


void    macTxSetPtr( uint16_t offset )
{
    uint16_t addr = 0x1900 + 1 + offset;

    encBankSelect( 0 );
    encWriteRegister( R_EWRPTL, addr & 0xFF );
    encWriteRegister( R_EWRPTH, ( addr >> 8 ) & 0xFF );
}


void    macTxSetLen( uint16_t bytes )
{
    // set end of packet pointer
    uint16_t eop = 0x1900 + bytes;

    encBankSelect( 0 );
    encWriteRegister( R_ETXNDL, eop & 0xFF );
    encWriteRegister( R_ETXNDH, ( eop >> 8 ) & 0xFF );
}


void    macTxSend()
{
    encBitClear( R_EIR, B_TXIF );
    encBitSet( R_ECON1, B_TXRTS );
}


uint8_t         macRxPoll()
{
    // get packet count
    uint8_t pcnt = 0;
    encBankSelect( 1 );
    encReadRegister( R_EPKTCNT, &pcnt );
    return pcnt;
}


void    macRxBegin()
{
    // read header
    // [0] Next Packet Pointer LSB
    // [1] Next Packet Pointer MSB
    // [2] Length LSB
    // [3] Length MSB
    // [4] Status 23:16
    // [5] Status 31:24
    encReadBuffer( gRxHeader, 6 );
    gRxLength = gRxHeader[2] + 256U * gRxHeader[3];
}


void    macRxRead( uint8_t *dst, uint16_t bytes )
{
    encReadBuffer( dst, bytes );
}


uint16_t macRxGetLen()
{
    return gRxLength;
}


void    macRxEnd()
{
    uint16_t npp = ((uint16_t)gRxHeader[1] << 8 ) | (uint16_t) gRxHeader[0];
    uint16_t rxp = 0;
    if ( npp == 0x0000 ) {
        rxp = 0x18FF; // Receive Buffer End ERXND
    } else {
        rxp = npp - 1;
    }
    encBankSelect( 0 );
    encWriteRegister( R_RXRDPTL, rxp & 0xFF );
    encWriteRegister( R_RXRDPTH, (rxp >> 8 ) & 0xFF );

    // set Buffer Read Pointer
    encWriteRegister( R_ERDPTL, gRxHeader[0] );
    encWriteRegister( R_ERDPTH, gRxHeader[1] );

    // decrement Packet Counter
    encBitSet( R_ECON2, B_PKTDEC );
}

