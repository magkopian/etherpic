//** pxnet.c **//

#include "pxcommon.h"
#include "pxmac.h"
#include "pxnet.h"

#ifndef USE_STATIC_IP
#include "pxdhcp.h"
#endif

#if defined(__XC8) || defined(__PICC__)
#define FIX_XC8_BUGS
#endif

static uint16_t gUdpTxBytes   = 0;

static ArpRecord arpTable[kMAXARPRECORDS];
static uint8_t arpTableIndex = 0;

void setu16 ( uint8_t* dst, uint16_t u16 ) {
    
    memcpy( (void*) dst, (void*) &u16, 2 );
    
}

void setu32 ( uint8_t *dst, uint32_t u32 ) {
    
    memcpy( (void*) dst, (void*) &u32, 4 );
    
}

uint16_t getu16 ( uint8_t *src ) {
    
    uint16_t result;
    
    memcpy( (void*) &result, (void*) src, 2 );
    return result;
    
}

uint32_t getu32 ( uint8_t *src ) {
    
    uint32_t result;
    
    memcpy( (void*) &result, (void*) src, 4 );
    return result;
    
}

uint16_t htons ( uint16_t n ) {
    
    uint16_t r = 0;
    uint8_t *src = (uint8_t*) &n;
    uint8_t *dst = (uint8_t*) &r;
    dst[0] = src[1];
    dst[1] = src[0];
    
    return r;
    
}

uint32_t htonl ( uint32_t n ) {
    
    uint32_t r = 0;
    uint8_t *src = (uint8_t*) &n;
    uint8_t *dst = (uint8_t*) &r;
    dst[0] = src[3];
    dst[1] = src[2];
    dst[2] = src[1];
    dst[3] = src[0];
    
    return r;
    
}

static uint32_t gCalcSum = 0;
static bool	gEvenByte = false;

static void clrChkSum ( void ) {
    
    gCalcSum = 0;
    gEvenByte = false;

}

static void addChkSum ( const void *dataPtr, uint16_t dataLen ) {
    
    const uint8_t* ptr = (const uint8_t *) dataPtr;
    
    while ( dataLen > 0 ) {
        
        if ( gEvenByte ) {
            gCalcSum += 256U * *ptr;
        }
        else {
            gCalcSum += *ptr;
        }
        
        ptr++;
        dataLen--;
        gEvenByte = ! gEvenByte;
        
    }
    
}

static uint16_t calcChkSum ( void ) {
    
    while ( gCalcSum >> 16 ) {
        gCalcSum = ( gCalcSum & 0xFFFF ) + ( gCalcSum >> 16 );
    }
    
    return ~gCalcSum;
    
}

uint16_t cksum( const void *src, int len ) {
    
    clrChkSum();
    addChkSum( src, len );
    
    return calcChkSum();
    
}

static void addEthHeader ( char* dst, uint16_t etype ) {
    
    memset( buff, 0, 14 );

#ifdef FIX_XC8_BUGS
    // NOTE: Workaround XC8 PIC16F pointer bug
    buff[0] = dst[0];
    buff[1] = dst[1];
    buff[2] = dst[2];
    buff[3] = dst[3];
    buff[4] = dst[4];
    buff[5] = dst[5];

    buff[6]  = gMyMAC[0];
    buff[7]  = gMyMAC[1];
    buff[8]  = gMyMAC[2];
    buff[9]  = gMyMAC[3];
    buff[10] = gMyMAC[4];
    buff[11] = gMyMAC[5];
#else
    memcpy( buff, dst, kMACLEN );
    memcpy( buff + 6, gMyMAC, kMACLEN );
#endif

    setu16( buff + 12, htons( etype ) );
    macTxWrite( buff, 14 );
}

static void sendARPRequest ( TIpAddr targetIP ) {
    
    uint8_t arpmsg[] = { 0x00, 0x01, 0x08, 0x00, 0x06, 0x04, 0x00, 0x01 };
    uint8_t bcMAC[kMACLEN] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    
    macTxBegin();

    // ethernet header
    addEthHeader( bcMAC, ETYPE_ARP );
    
    memset( buff, 0, 20 );
    memcpy( buff, arpmsg, 8 );
    memcpy( buff + 8, gMyMAC, kMACLEN );
    memcpy( buff + 14, gMyIP, kIPLEN );
    macTxWrite( buff, 18 );
    
    memset( buff, 0, 20 );
    memset( buff, 0, kMACLEN ); // Since we don't know the target MAC we fill it with zeros
    memcpy( buff + 6, targetIP, kIPLEN );
    macTxWrite( buff, 10 );
    
    macTxSetLen( ETH_HEADER_LEN + 28 );
    macTxSend();
    
}

static void handleARP ( TPacket* pkt ) {
    
    // htype=1 ptype=0x0800 hlen=6 plen=4 op=1
    uint8_t arpmsg[] = { 0x00, 0x01, 0x08, 0x00, 0x06, 0x04, 0x00, 0x01 };
    TMacAddr SHA;

    // ARP request header - 28 bytes
    // check first 8 bytes
    macRxRead( buff, 8 );

    // Ignore non IPv4 ARP requests
    if ( memcmp( buff, arpmsg, sizeof( arpmsg ) - 1 ) ) return;
    
    if ( buff[7] == 0x02 ) { // ARP Reply
        
        // next 20 bytes of header
        // SHA=buff+0, SPA=buff+6, THA=buff+10, TPA=buff+16
        macRxRead( buff, 20 );
        
        memcpy( arpTable[arpTableIndex].mac_addr, buff + 0, kMACLEN );
        memcpy( arpTable[arpTableIndex++].ip_addr, buff + 6, kIPLEN );
        
        // Ignore requests that aren't for our IP
        if ( memcmp( buff + 16, gMyIP, sizeof( TIpAddr ) ) ) return;
        
        if ( arpTableIndex > kMAXARPRECORDS ) arpTableIndex = 0;
        
    }
    else if ( buff[7] == 0x01 ) { // ARP Request
        
        // next 20 bytes of header
        // SHA=buff+0, SPA=buff+6, THA=buff+10, TPA=buff+16
        macRxRead( buff, 20 );

        memcpy( SHA, buff + 0, kMACLEN );

        // Ignore requests that aren't for our IP
        if ( memcmp( buff + 16, gMyIP, sizeof( TIpAddr ) ) ) return;

        macTxBegin();

        // ethernet header
        addEthHeader( SHA, ETYPE_ARP );

        // ARP reply message is 18 bytes
        memset( buff, 0, 18 );
        memcpy( buff, arpmsg, 8 );
        buff[7] = 2; // ARP reply
        memcpy( buff + 8, gMyMAC, kMACLEN );
        memcpy( buff + 14, gMyIP, kIPLEN );
        macTxWrite( buff, 18 );

        macTxSetLen( ETH_HEADER_LEN + 18 );
        macTxSend();
    
    }

}

static void handleICMP ( TPacket* pkt ) {
    
    uint8_t type, code;
    uint16_t id, seq, left;
    uint16_t icmp_len = pkt->ip_len;

    // read ICMP header - 8 bytes
    memset( buff, 0, 8 );
    macRxRead( buff, 8 );

    type = buff[0];
    code = buff[1];
    id = ntohs( getu16( buff + 4 ) );
    seq = ntohs( getu16( buff + 6 ) );

    if ( type != 8 ) return;

    left = icmp_len - 8;

    macTxBegin();

    // ethernet header
#ifdef FIX_XC8_BUGS
    // NOTE: Workaround XC8 PIC16F pointer bug
    TMacAddr pkt_en_srcMac;
    pkt_en_srcMac[0] = pkt->en_srcMac[0];
    pkt_en_srcMac[1] = pkt->en_srcMac[1];
    pkt_en_srcMac[2] = pkt->en_srcMac[2];
    pkt_en_srcMac[3] = pkt->en_srcMac[3];
    pkt_en_srcMac[4] = pkt->en_srcMac[4];
    pkt_en_srcMac[5] = pkt->en_srcMac[5];
    addEthHeader( pkt_en_srcMac, ETYPE_IPV4 );
#else
    addEthHeader( pkt->en_srcMac, ETYPE_IPV4 );
#endif

    // IPv4 header - 20 bytes
    memset( buff, 0, 20 );
    buff[0] = 0x45; // ver=4 ihl=5
    setu16( buff + 2, htons( 20 + icmp_len ) );
    buff[6] = 0x40; // DF flag
    buff[8] = DEFAULT_TTL;
    buff[9] = IPV4_PROTO_ICMP;
    memcpy( buff + 12, gMyIP, kIPLEN );

#ifdef FIX_XC8_BUGS
    // NOTE: Workaround XC8 PIC16F pointer bug
    buff[16] = pkt->ip_srcIp[0];
    buff[17] = pkt->ip_srcIp[1];
    buff[18] = pkt->ip_srcIp[2];
    buff[19] = pkt->ip_srcIp[3];
#else
    memcpy( buff + 16, pkt->ip_srcIp, kIPLEN );
#endif

    setu16( buff + 10, cksum( buff, 20 ) );
    macTxWrite( buff, 20 );

    // copy remaining ICMP message from Rx to Tx
    macTxSetPtr( ETH_HEADER_LEN + 20 + 8 );
    clrChkSum();
    
    while ( left > 0 ) {
        uint16_t bytes = left;
        if ( bytes > 20 ) bytes = 20;
        macRxRead( buff, bytes );
        macTxWrite( buff, bytes );
        addChkSum( buff, bytes );
        left -= bytes;
    }

    // write new ICMP header
    memset( buff, 0, 8 );
    setu16( buff + 4, htons( id ) );
    setu16( buff + 6, htons( seq ) );
    addChkSum( buff, 8 );
    setu16( buff + 2, calcChkSum() );
    macTxSetPtr( ETH_HEADER_LEN + 20 );
    macTxWrite( buff, 8 );

    macTxSetLen( ETH_HEADER_LEN + 20 + icmp_len );
    macTxSend();
    
}

void handleUDP ( TPacket* pkt ) {
    
    uint16_t remPort, locPort;
    uint16_t udp_len;
    uint16_t udp_checksum;

    // TODO: Implement checksum check
    
    // read UDP header - 8 bytes
    memset( buff, 0, 8 );
    macRxRead( buff, 8 );
    
    remPort = ntohs( getu16( buff ) );
    locPort = ntohs( getu16( buff + 2 ) );
    udp_len = ntohs( getu16( buff + 4 ) );
    udp_checksum = ntohs( getu16( buff + 6 ) );

    #ifndef USE_STATIC_IP
    if ( remPort == 67 && locPort == 68 ) {
        handleDHCP(pkt);
    }
    #endif
    
}

static TSocket 	gSockets[ kMAXSOCKETS ];
static bool	gInOnData  = false;
static uint8_t	gInSock	   = 0;

static int8_t findSocket( uint16_t locPort, uint16_t remPort, char* remIp ) {
    
    sock_t sock;

    for ( sock = 0; sock < kMAXSOCKETS; sock++ ) {
	TSocket *srec = gSockets + sock;

#ifdef FIX_XC8_BUGS
        // NOTE: work around XC8/PIC16F pointer bug
        TIpAddr srec_remIp;
        srec_remIp[0] = srec->remIp[0];
        srec_remIp[1] = srec->remIp[1];
        srec_remIp[2] = srec->remIp[2];
        srec_remIp[3] = srec->remIp[3];
	if ( ( srec->locPort == locPort ) &&
	     ( srec->remPort == remPort ) &&
	     ( memcmp( srec_remIp, remIp, kIPLEN ) == 0 ) )
	    return sock;
#else
	if ( ( srec->locPort == locPort ) &&
	     ( srec->remPort == remPort ) &&
	     ( memcmp( srec->remIp, remIp, kIPLEN ) == 0 ) )
	    return sock;
#endif
    }
    
    return -1;
    
}

static int8_t checkEphemeralPort ( uint16_t port ) {
    
    sock_t sock;
    TSocket *srec;
    
    for ( sock = 0, srec = gSockets; sock < kMAXSOCKETS && srec->locPort != port; srec = gSockets + ++sock );
    
    if ( srec->locPort == port ) {
        return -1;
    }
    
    return 0;
    
}

static int8_t newSocket ( void ) {
    
    sock_t sock;

    for ( sock = 0; sock < kMAXSOCKETS; ++sock ) {
        
        if ( gSockets[sock].state == CLOSED ) {
            return sock;
        }
        
    }
    
    return -1;
    
}

static void callDisconnect ( sock_t sock, TSocket *srec ) {
    
    if ( srec->callbacks & CBF_CONNECT ) {
        
        if ( ( srec->callbacks & CBF_DISCONNECT ) == 0 ) {
            srec->callbacks |= CBF_DISCONNECT;
        }
        
    }
    
}

static void closeSocket ( sock_t sock ) {
    
    TSocket *srec = gSockets + sock;
    
    srec->state = CLOSED;
    callDisconnect( sock, srec );
    
}

static uint16_t gTcpTxBytes = 0;

static void tcpTxInit ( TSocket *srec ) {
    
    macTxBegin();

    // ethernet header
    addEthHeader( srec->remMac, ETYPE_IPV4 );

    // setup for tcpSendWrite()
    macTxSetPtr( ETH_HEADER_LEN + 20 + 20 );
    clrChkSum();
    gTcpTxBytes = 0;

}

static void tcpTxAppend ( TSocket *srec, const void *dataPtr, uint16_t dataLen ) {
    
    macTxWrite( dataPtr, dataLen );
    addChkSum( dataPtr, dataLen );
    gTcpTxBytes += dataLen;

}

static uint16_t tcpTxFinalize ( TSocket *srec, uint8_t flags ) {
    
    uint16_t wsize;

    // Pseudo-IP-header for TCP checksum - 12 bytes
    memset( buff, 0, 12 );
    memcpy( buff, gMyIP, kIPLEN );
    memcpy( buff + 4, srec->remIp, kIPLEN );
    buff[9] = IPV4_PROTO_TCP;
    setu16( buff + 10, htons( 20 + gTcpTxBytes ) );

    gEvenByte = false; // in case dataLen is odd
    addChkSum( buff, 12 );

    // TCP Header - 20 bytes
    memset( buff, 0, 20 );
    setu16( buff, htons( srec->locPort ) );
    setu16( buff + 2, htons( srec->remPort ) );
    setu32( buff + 4, htonl( srec->locSeqNo ) );
    setu32( buff + 8, htonl( srec->remSeqNo ) );
    buff[ 12 ] = 0x50; // header size = 5 x 32 bits = 20 bytes
    buff[ 13 ] = flags;

    // Receive window = MAC Free buffer size - TCP header (20)
    //                - IP header (20) - Ethernet header (14)

    // limit one segment at a time from remote host to avoid retries

    wsize = macRxGetBufferFree();
    if ( wsize > ETH_MTU ) wsize = ETH_MTU;
    if ( wsize > 54 ) wsize -= 54;
    else wsize = 0;
    setu16( buff + 14, htons( wsize ) );

    // Calculate TCP checksum
    addChkSum( buff, 20 );
    setu16( buff + 16, calcChkSum() );

    // write TCP header
    macTxSetPtr( ETH_HEADER_LEN + 20 );
    macTxWrite( buff, 20 );

    // IPv4 header - 20 bytes
    memset( buff, 0, 20 );
    buff[0] = 0x45; // ver=4 ihl=5
    setu16( buff + 2, htons( 20 + 20 + gTcpTxBytes ) );
    buff[6] = 0x40; // DF flag
    buff[8] = DEFAULT_TTL;
    buff[9] = IPV4_PROTO_TCP;
    memcpy( buff + 12, gMyIP, kIPLEN );
    memcpy( buff + 16, srec->remIp, kIPLEN );
    setu16( buff + 10, cksum( buff, 20 ) );

    // write IP header
    macTxSetPtr( ETH_HEADER_LEN );
    macTxWrite( buff, 20 );

    macTxSetLen( ETH_HEADER_LEN + 20 + 20 + gTcpTxBytes );
    macTxSend();

    return gTcpTxBytes;
    
}

static void sendTcpPacket ( TSocket *srec, uint8_t flags, const void *dataPtr, uint16_t dataLen ) {
    
    tcpTxInit( srec );
    
    if ( dataPtr && dataLen ) {
        tcpTxAppend( srec, dataPtr, dataLen );
    }
    
    tcpTxFinalize( srec, flags );
    
}

uint16_t data_pos = 0, data_start = 0, status = 0;

static void handleHttp ( sock_t sock, uint16_t dlen, bool final_pkt ) {
    
    uint16_t i, j, k;
    uint16_t bytes_read = 0;
    char *header;
    
    gInOnData = true;
    gInSock = sock;
    
    i = 0;
    do {
        if ( i + 100 >= dlen ) {
            bytes_read = dlen - i;
        }
        else {
            bytes_read = 100; 
        }
        
        netRead(sock, buff, bytes_read);
        
        for ( j = 0; j < bytes_read && data_start != 4; ++j ) {
            
            if ( buff[j] == '\r' && data_start == 0 ) {
                data_start = 1;
            }
            else if ( buff[j] == '\n' && data_start == 1 ) {
                 data_start = 2;
            }
            else if ( buff[j] == '\r' && data_start == 2 ) {
                 data_start = 3;
            }
            else if ( buff[j] == '\n' && data_start == 3 ) {
                 data_start = 4;
            }
            else {
                data_start = 0;
            }

        }
        
        // If we have reached the response data
        if ( data_start == 4 ) {
            memcpy(&data_buff[data_pos], &buff[j], bytes_read - j);
            data_pos += bytes_read - j;
        }
        else { // If we are still in the headers section  
            
            if ( status == 0 && ((header = strstr(buff, "HTTP/1.1")) != NULL || (header = strstr(buff, "HTTP/1.0")) != NULL) ) {
                for ( k = 8; k < bytes_read; ++k ) {
                    if ( isdigit(header[k]) ) {
                        status = strtoul(&header[k], NULL, 10);
                        break;
                    }
                }
            }
            
        }
        
        i += 100;
    } while ( i < dlen );
    
    gInOnData = false;
    
    // If that's the final packet we forward the data to the application
    if ( final_pkt == true ) {
        data_buff[data_pos] = '\0';
        data_start = 0;
        data_pos = 0;
        onHttpResponse(status, (char *) (data_buff));
        status = 0;
    }

}

static void handleTCP ( TPacket* pkt ) {
    
    uint8_t offset, flags, options;
    uint16_t srcPort, dstPort, wsize;
    uint32_t seqNo, ackNo;
    sock_t sock;

    // A TCP header without options is 20 bytes
    macRxRead( buff, 20 );

    srcPort = ntohs( getu16( buff ) );
    dstPort = ntohs( getu16( buff + 2 ) );
    seqNo = ntohl(  getu32( buff + 4 ) );
    ackNo = ntohl(  getu32( buff + 8 ) );

    offset = buff[12] >> 4;
    flags = buff[13];
    wsize = ntohs( getu16( buff + 14 ) );

    // Discard the options
    options = offset - 5;
    
    while ( options > 0 ) {
        macRxRead( buff, 4 );
        options--;
    }

    #ifdef FIX_XC8_BUGS
        // NOTE: work around XC8/PIC16F pointer bug
        TIpAddr pkt_ip_srcIp;
        TMacAddr pkt_en_srcMac;

        pkt_ip_srcIp[0] = pkt->ip_srcIp[0];
        pkt_ip_srcIp[1] = pkt->ip_srcIp[1];
        pkt_ip_srcIp[2] = pkt->ip_srcIp[2];
        pkt_ip_srcIp[3] = pkt->ip_srcIp[3];

        pkt_en_srcMac[0] = pkt->en_srcMac[0];
        pkt_en_srcMac[1] = pkt->en_srcMac[1];
        pkt_en_srcMac[2] = pkt->en_srcMac[2];
        pkt_en_srcMac[3] = pkt->en_srcMac[3];
        pkt_en_srcMac[4] = pkt->en_srcMac[4];
        pkt_en_srcMac[5] = pkt->en_srcMac[5];

        sock = findSocket( dstPort, srcPort, pkt_ip_srcIp );
    #else
        sock = findSocket( dstPort, srcPort, pkt->ip_srcIp );
    #endif
    
    // TODO: Check if sequence and acknowledgment numbers have the expected values
        
    if ( ( sock >= 0 ) && ( flags == (TCP_FLAG_SYN | TCP_FLAG_ACK) ) ) { // If the socket exists and we receive SYN-ACK
        
        TSocket *srec = gSockets + sock;
        
        // Drop packet if TCP state is not SYN_SENT
        if ( srec->state != SYN_SENT ) {
            return;
        }
        
        srec->locPort = dstPort;
        srec->remPort = srcPort;
        
        #ifdef FIX_XC8_BUGS
            memcpy( srec->remIp, pkt_ip_srcIp, kIPLEN );
            memcpy( srec->remMac, pkt_en_srcMac, kMACLEN );
        #else
            memcpy( srec->remIp, pkt->ip_srcIp, kIPLEN );
            memcpy( srec->remMac, pkt->en_srcMac, kMACLEN );
        #endif

        srec->remSeqNo = seqNo + 1; // Acknowledgment number
        srec->locSeqNo = ackNo;
        
        // We respond to the SYN-ACK with an ACK
        sendTcpPacket( srec, TCP_FLAG_ACK, NULL, 0 );

        // And since now the TCP connection has been established,
        srec->state = ESTABLISHED;
        
        if ( srec->remPort == 80 ) {
            onHttpConnect(sock); // We send the HTTP request
            srec->state = DATA_SENT_WAIT; // After sending the request we wait for ACK
        }
            
    }
    else if ( ( sock >= 0 ) && ( flags & TCP_FLAG_RST ) ) { // If the server has send a RST
        
        closeSocket( sock );
        
    }
    else if ( ( sock >= 0 ) && ( flags == TCP_FLAG_ACK || (( flags & TCP_FLAG_PSH ) && ( flags & TCP_FLAG_ACK )) ) ) { // If the server has send an ACK or a PSH-ACK
        
        TSocket *srec = gSockets + sock;
        uint16_t dlen;
        
        if ( srec->state == DATA_SENT_WAIT ) { // Waiting for ACK of the sent data
            srec->state = ESTABLISHED; // Since we received the ACK we were waiting for, we set the state back to ESTABLISHED
            return;
        }
        else if ( srec->state == FIN_WAIT_2 ) { // Waiting for last ACK to finish the end handshake
            closeSocket(sock); // Close the socket
            return;
        }
        else if ( srec->state != ESTABLISHED ) {
            return;
        }
        
        // At this point if the state is ESTABLISHED and we have already received an ACK for the data we sent
        // to the server, it means that the server is now sending data to us
        
        // Calculate the data length
        dlen = pkt->ip_len - ( offset * 4 );
        
        srec->locPort = dstPort;
        srec->remPort = srcPort;
        
        if ( dlen > 0 && srec->protocol == TCP_PROTO_HTTP ) {
            // If the PSH flag is also set, that means this is the last data packet
            if ( flags & TCP_FLAG_PSH ) {
                handleHttp(sock, dlen, true);
            }
            else {
                handleHttp(sock, dlen, false);
            }
        }
        
#ifdef FIX_XC8_BUGS
        memcpy( srec->remIp, pkt_ip_srcIp, kIPLEN );
        memcpy( srec->remMac, pkt_en_srcMac, kMACLEN );
#else
        memcpy( srec->remIp, pkt->ip_srcIp, kIPLEN );
        memcpy( srec->remMac, pkt->en_srcMac, kMACLEN );
#endif
        srec->remSeqNo = seqNo + dlen;
        srec->locSeqNo = ackNo;
        
        //sendTcpPacket( srec, TCP_FLAG_ACK, NULL, 0 );
        
        // And if that was the last data packet, we start waiting for the FIN-ACK from the server
        if ( (flags & TCP_FLAG_PSH) && (flags & TCP_FLAG_FIN) ) {
            sendTcpPacket( srec, TCP_FLAG_ACK, NULL, 0 );
            ++srec->remSeqNo;
            sendTcpPacket( srec, TCP_FLAG_FIN | TCP_FLAG_ACK, NULL, 0 );
            srec->state = FIN_WAIT_2;
        }
        else if ( flags & TCP_FLAG_PSH ) {
            sendTcpPacket( srec, TCP_FLAG_ACK, NULL, 0 );
            srec->state = FIN_WAIT_1;
        }
        else {
            sendTcpPacket( srec, TCP_FLAG_ACK, NULL, 0 );
        }
        
        // TODO: Resend packet if ACK is not received
        
    }
    else if ( ( sock >= 0 ) && ( flags & TCP_FLAG_FIN ) && ( flags & TCP_FLAG_ACK ) ) { // When server sends a FIN-ACK to terminate the session
        
        TSocket *srec = gSockets + sock;
        
        // If TCP state is not FIN_WAIT_1 drop the packet
        if ( srec->state != FIN_WAIT_1 ) {
            return;
        }
            
        srec->locPort = dstPort;
        srec->remPort = srcPort;
        
        #ifdef FIX_XC8_BUGS
            memcpy( srec->remIp, pkt_ip_srcIp, kIPLEN );
            memcpy( srec->remMac, pkt_en_srcMac, kMACLEN );
        #else
            memcpy( srec->remIp, pkt->ip_srcIp, kIPLEN );
            memcpy( srec->remMac, pkt->en_srcMac, kMACLEN );
        #endif

        srec->remSeqNo = seqNo + 1;
        srec->locSeqNo = ackNo;
        
        // Respond to FIN-ACK with a FIN-ACK to accept
        sendTcpPacket( srec, TCP_FLAG_FIN | TCP_FLAG_ACK, NULL, 0 );

        // Wait for the last ACK from the server
        srec->state = FIN_WAIT_2;
        
    }

}

static void handleIPV4 ( TPacket* pkt ) {
    uint8_t version, ihl, protocol;
    uint16_t chksum;
    int8_t options;

    // IPv4 header without options is 20 bytes
    macRxRead( buff, 20 );

    version = buff[0] >> 4;
    ihl = buff[0] & 0x0F;
    pkt->ip_len = ntohs( getu16( buff + 2 ) );

    pkt->ip_ttl = buff[8];
    protocol = buff[9];
    chksum = getu16( buff + 10 );

    pkt->ip_len -= ( ihl * 4 );

    if ( version != 4 )
	return;

    memcpy( pkt->ip_srcIp, buff + 12, kIPLEN );
    memcpy( pkt->ip_dstIp, buff + 16, kIPLEN );

    // Discard options
    options = ihl - 5;
    while ( options > 0 ) {
        macRxRead( buff, 4 );
        --options;
    }

    if ( protocol == IPV4_PROTO_ICMP ) {
        handleICMP(pkt);
    }
    else if ( protocol == IPV4_PROTO_TCP ) {
        if ( pkt->en_dest == DEST_UNICAST ) {
            handleTCP(pkt);
        }
    }
    else if ( protocol == IPV4_PROTO_UDP) {
        handleUDP(pkt);
    }

}

static void handlePacket ( void ) {
    uint8_t bcMAC[kMACLEN] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    uint8_t mcMAC[] = { 0x01, 0x00, 0x5E };
    TPacket pkt;

    // Ethernet Frame header
    // Dst MAC, Src MAC, EtherType
    // 6 + 6 + [4] + 2 = 16

    macRxRead( buff, 14 );

    memcpy( pkt.en_dstMac, buff, kMACLEN );
    memcpy( pkt.en_srcMac, buff + 6, kMACLEN );
    pkt.en_etype = ntohs( getu16( buff + 12 ) );

    // handle 802.1Q frame
    if ( pkt.en_etype == ETYPE_802_1Q ) {
        macRxRead( buff, 4 );
        pkt.en_etype = ntohs( getu16( buff + 2 ) );
    }

    pkt.en_dest = DEST_OTHER;
    if ( memcmp( pkt.en_dstMac, gMyMAC, kMACLEN ) == 0 ) {
        pkt.en_dest = DEST_UNICAST;
    }
    else if ( memcmp( pkt.en_dstMac, bcMAC, kMACLEN ) == 0  ){
        pkt.en_dest = DEST_BROADCAST;
    }
    else if ( memcmp( pkt.en_dstMac, mcMAC, 3 ) == 0 ) {
        pkt.en_dest = DEST_MULTICAST;
    }

    // Packets that are neither IPv4 or ARP are dropped
    if ( pkt.en_dest ) {
        if ( pkt.en_etype == ETYPE_IPV4 ) {
            handleIPV4( &pkt );
        }
        else if ( pkt.en_etype == ETYPE_ARP ) {
            handleARP( &pkt );
        }
    }
    
}

void netInit ( void ) {
    
    memset( &gSockets, 0, sizeof( gSockets ) );
    macInit();
    
}

void netEnable ( void ) {
    
    macEnable();
    
}

void netIdle ( void ) {
    
    uint8_t count = macRxPoll();
    
    if ( count > 0 ) {
        macRxBegin();
        handlePacket();
        macRxEnd();
    }
    
}

void netTimer ( uint32_t seconds ) {
    
    sock_t sock;
    
    // expire any inactive sockets and sockets in TIME_WAIT
    for ( sock = 0; sock < kMAXSOCKETS; ++sock ) {
        
        TSocket *srec = gSockets + sock;
        if ( srec->state != CLOSED ) {

            srec->timer++;

            if ( srec->timer >= kINACTIVE_SECS ) {
                sendTcpPacket( srec, TCP_FLAG_RST, NULL, 0 );
                closeSocket( sock );
            }
            else if ( srec->state != ESTABLISHED && srec->timer >= kTIME_WAIT_SECS ) {
                
                // Delete record from Arp Table
                for ( int i = 0; i < arpTableIndex; ++i ) {
                    if ( memcmp(arpTable[i].ip_addr, srec->remIp, kIPLEN) == 0 ) {
                        memcpy(arpTable[i].ip_addr, 0, kIPLEN);
                        memcpy(arpTable[i].mac_addr, 0, kMACLEN);
                    }
                }
                
                closeSocket( sock );
            }

        }
    
    }
    
}

void netRead ( sock_t sock, uint8_t* dst, uint16_t bytes ) {
    
    if ( gInOnData ) {
        
        if ( gInSock == sock ) {
            
            TSocket *srec = gSockets + sock;
            
            if ( srec->state == ESTABLISHED ) {
                macRxRead( dst, bytes );
            }
            
        }
        
    }
    
}

void netClose ( sock_t sock ) {
    
    TSocket *srec = gSockets + sock;
    
    if ( srec->state == ESTABLISHED ) {
        
        sendTcpPacket( srec, TCP_FLAG_FIN | TCP_FLAG_ACK, NULL, 0 );
        srec->state = FIN_WAIT_1;
        srec->locSeqNo++;
        
    }
    
}

void netGetAddr ( int8_t sock, uint16_t* port, uint8_t* ip, uint8_t* mac, uint8_t* state ) {
    
    TSocket *srec = gSockets + sock;
    
    *port = srec->remPort;
    memcpy( ip, srec->remIp, kIPLEN );
    memcpy( mac, srec->remMac, kMACLEN );
    *state = srec->state;
    
}

void netPutBegin ( sock_t sock ) {
    
    TSocket *srec = gSockets + sock;
    tcpTxInit( srec );
    
}

void netPutAppend ( sock_t sock, const uint8_t *src, uint16_t bytes ) {
    
    TSocket *srec = gSockets + sock;

    const uint8_t *ptr = (const uint8_t *) src;
    int16_t left = bytes;
#ifdef FIX_XC8_BUGS
    char buffer[20];
#endif

    while ( left > 0 ) {
        int16_t free = ETH_MTU - gTcpTxBytes - 54;
        uint16_t count = left;
#ifdef FIX_XC8_BUGS
        if ( count > sizeof( buffer ) )
            count = sizeof( buffer );
#endif
        if ( count > free )
            count = free;

#ifdef FIX_XC8_BUGS
        // NOTE: work around XC8/PIC18F pointer bug
        memcpy( buffer, ptr, count );
        tcpTxAppend( srec, buffer, count );
#else
        tcpTxAppend( srec, ptr, count );
#endif

        left -= count;
        free -= count;
        ptr  += count;

        if ( free <= 0 ) {
            netPutFinalize( sock );
            netPutBegin( sock );
        }
    }
    
}


void netPutFinalize ( sock_t sock ) {
    
    TSocket *srec = gSockets + sock;
    uint16_t bytes = tcpTxFinalize( srec, TCP_FLAG_PSH | TCP_FLAG_ACK );
    srec->locSeqNo += bytes;
    
}


void netWrite ( sock_t sock, const uint8_t *src, uint16_t bytes ) {
    
    netPutBegin( sock );
    netPutAppend( sock, src, bytes );
    netPutFinalize( sock );

}

int8_t arpTableLookup ( TIpAddr ip_addr, TMacAddr *mac_addr ) {
    
    int8_t i;
    
    for ( i = 0; i <= arpTableIndex; ++i ) {
        
        if ( memcmp(arpTable[i].ip_addr, ip_addr, kIPLEN) == 0 ) {
            memcpy(mac_addr, arpTable[i].mac_addr, kMACLEN);
            break;
        }
        
    }
    
    if ( i > arpTableIndex ) {
        return -1;
    }
    
    return 0;
    
}

sock_t tcpEstablishConnection ( TIpAddr ip_remIp, uint16_t dstPort, uint16_t srcPort, uint8_t protocol ) {
    
    uint32_t seqNo;
    sock_t sock;
    TMacAddr en_remMac;
    
    if ( arpTableLookup(ip_remIp, (TMacAddr *) &en_remMac) == -1 ) {
        sendARPRequest ( ip_remIp );
        return -1;
    }
    
    sock = newSocket();
    
	if ( sock >= 0 ) {
        
	    TSocket *srec = gSockets + sock;

        srec->callbacks = 0;
        srec->timer = 0;
        
        if ( srcPort > MAX_EPHEMERAL_SOCKET || srcPort < MIN_EPHEMERAL_SOCKET ) srcPort = 0;
        
        while ( srcPort == 0 ) {
            
            srcPort = rand() % (MAX_EPHEMERAL_SOCKET + 1 - MIN_EPHEMERAL_SOCKET) + MIN_EPHEMERAL_SOCKET;
            
            if ( checkEphemeralPort(srcPort) == -1 ) {
                srcPort = 0;
            }
            
        }
        
        srec->protocol = protocol; // Application protocol
        
	    srec->locPort = srcPort; // Source TCP Port
	    srec->remPort = dstPort; // Destination TCP Port

	    memcpy( srec->remIp, ip_remIp, kIPLEN ); // Destination IP Address
	    memcpy( srec->remMac, en_remMac, kMACLEN ); // Destination MAC Address

        do {
            seqNo = rand();
            seqNo = (seqNo << 16) | rand();
        } while ( seqNo == 0 );
        
        srec->locSeqNo = seqNo; // Sequence number
        srec->remSeqNo = 0; // Acknowledgment number
        
	    // Send SYN Packet
	    sendTcpPacket( srec, TCP_FLAG_SYN, NULL, 0 );
        
	    srec->locSeqNo++;
        srec->state = SYN_SENT;

	}
    else {
        return -1;
    }
    
    return sock;
    
}

void udpTxInit ( USocket *srec ) {
    
    macTxBegin();

    // Ethernet header
    addEthHeader( srec->remMac, ETYPE_IPV4 );

    macTxSetPtr( ETH_HEADER_LEN + 8 + 20 );

    gUdpTxBytes = 0;

}

void udpTxAppend ( const void *dataPtr, uint16_t dataLen ) {
    
    // write data
    macTxWrite( dataPtr, dataLen );
    gUdpTxBytes += dataLen;
    
}

uint16_t udpTxFinalize( USocket *srec ) {

    // UDP header - 8 bytes
    memset( buff, 0, 8 );
    setu16( buff, htons( srec->locPort ) );
    setu16( buff + 2, htons( srec->remPort ) );
    setu16( buff + 4, htons( 8 + gUdpTxBytes ) );

    // TODO: Implement Checksum calculation
    
    // write UDP header
    macTxSetPtr( ETH_HEADER_LEN + 20 );
    macTxWrite( buff, 8 );

    // IPv4 header - 20 bytes
    memset( buff, 0, 20 );
    buff[0] = 0x45; // ver=4 ihl=5
    setu16( buff + 2, htons( 20 + 8 + gUdpTxBytes ) );
    buff[6] = 0x40; // DF flag
    buff[8] = DEFAULT_TTL;
    buff[9] = IPV4_PROTO_UDP;
    memcpy( buff + 12, srec->locIp, kIPLEN );
    memcpy( buff + 16, srec->remIp, kIPLEN );
    setu16( buff + 10, cksum( buff, 20 ) );

    // write IP header
    macTxSetPtr( ETH_HEADER_LEN );
    macTxWrite( buff, 20 );

    macTxSetLen( ETH_HEADER_LEN + 20 + 8 + gUdpTxBytes );
    macTxSend();

    return gUdpTxBytes;

}