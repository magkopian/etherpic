
//** pxenc.c **//

#include "pxcommon.h"
#include "pxspi.h"
#include "pxenc.h"


void encSetup( uint8_t flags )
{
    uint8_t u;

    // clear nCS and set pin to output
    ENC_nCS_LAT = 1;
    ENC_nCS_TRIS = 0;

    // setup the SPI port
    ENC_spiSetup( kSPI_MASTER  | kSPI_CLK_DIV_4 | kSPI_IDLE_LOW |
                  kSPI_TX_ATOI | kSPI_SAMP_MID );

    // wait for ENC28J60 oscillator startup timer
    while ( 1 ) {
        uint8_t es = 0;
        encReadRegister( R_ESTAT, &es );
        if ( es & B_CLKRDY )
            break;
    }

    // system reset
    encReset();
    __delay_ms( 1 );
    
    // enable Multicast
    encBankSelect( 1 );
    encBitSet( R_ERXFCON, B_MCEN );
    encBitSet( R_ERXFCON, B_UCEN );
    encBitSet( R_ERXFCON, B_BCEN );

    // set disable half-duplex loopback flag
    // in PHY register PHCON2 via MIIM
    u = 0;
    encBankSelect( 3 );
    do {
        encReadRegister( R_MISTAT, &u );
    } while ( u & B_BUSY );

    encBankSelect( 2 );
    encWriteRegister( R_MIREGADR, R_PHCON2 );
    encWriteRegister( R_MIWRL, 0x00 );
    encWriteRegister( R_MIWRH, B_HDLDIS );

    // set up Ethernet Buffer pointers
    // Note: See Silicon Errata for ENC28J60 B1,B4,B5,B7
    // Receive Buffer must start at 0x0000
    // RX Buffer 0x0000 - 0x18FF, TX Buffer 0x1900 - 0x1FFF
    // RX Read Pointer RXRDPTL/RXRDPTH must be odd
    // setting Receive Buffer Start sets RXWRPTL/RXWRPTH

    encBankSelect( 0 );
    // Buffer Read Pointer
    encWriteRegister( R_ERDPTL, 0x00 );
    encWriteRegister( R_ERDPTH, 0x00 );
    // Buffer Write Pointer
    encWriteRegister( R_EWRPTL, 0x00 );
    encWriteRegister( R_EWRPTH, 0x19 );
    // Transmit Buffer Start
    encWriteRegister( R_ETXSTL, 0x00 );
    encWriteRegister( R_ETXSTH, 0x19 );
    // Transmit Buffer End
    encWriteRegister( R_ETXNDL, 0xFF );
    encWriteRegister( R_ETXNDH, 0x1F );
    // Receive Buffer Start
    encWriteRegister( R_ERXSTL, 0x00 );
    encWriteRegister( R_ERXSTH, 0x00 );
    // Receive Buffer End
    encWriteRegister( R_ERXNDL, 0xFF );
    encWriteRegister( R_ERXNDH, 0x18 );
    // RX Read Pointer
    encWriteRegister( R_RXRDPTL, 0xFF );
    encWriteRegister( R_RXRDPTH, 0x18 );

    // enable MAC receive
    encBankSelect( 2 );
    encBitSet( R_MACON1, B_MARXEN );
    // enable padding and CRC
    encBitSet( R_MACON3, /*B_PADCFG2 | B_PADCFG1 |*/ B_PADCFG0 | B_TXCRCEN );

    // configure Back-to-Back Inter-Packet Gap register
    encWriteRegister( R_MABBIPG, 0x12 );
    // configure Non-Back-to-Back Inter-Packet Gap register
    encWriteRegister( R_MAIPGL, 0x12 );
    encWriteRegister( R_MAIPGH, 0x0C );
}


void encEnable()
{
    // enable receive
    encBitSet( R_ECON1, B_RXEN );
}


static uint8_t gLastBank = 0;

void encBankSelect( uint8_t banknum )
{
    uint8_t bset = banknum & ~ gLastBank;
    uint8_t bclr = gLastBank & ~ banknum;
    if ( bset ) encBitSet( R_ECON1, bset );
    if ( bclr ) encBitClear( R_ECON1, bclr );
    gLastBank = banknum;
}


void encReadRegister( uint8_t r, uint8_t* val )
{
    uint8_t cmd, reply;

    ENC_nCS_LAT = 0;
    Nop();

    cmd = 0b00000000 | ( r & 0b00011111 );
    reply = 0x00;

    ENC_spiXfer( cmd , &reply );
    ENC_spiXfer( 0x00, &reply );

    *val = reply;

    ENC_nCS_LAT = 1;
}


void encWriteRegister( uint8_t r, uint8_t val )
{
    uint8_t cmd, reply;

    ENC_nCS_LAT = 0;
    Nop();

    cmd = 0b01000000 | ( r & 0b00011111 );
    reply = 0x00;

    ENC_spiXfer( cmd, &reply );
    ENC_spiXfer( val, &reply );

    ENC_nCS_LAT = 1;
}


void encReadBuffer(  uint8_t* buffer, uint16_t count )
{
    uint16_t i;
    uint8_t cmd, reply;

    ENC_nCS_LAT = 0;
    Nop();

    cmd = 0b00111010;
    reply = 0x00;
    ENC_spiXfer( cmd, &reply );
    for ( i = 0; i < count; i++ )
        ENC_spiXfer( 0x00, &buffer[i] );

    ENC_nCS_LAT = 1;
}


void encWriteBuffer( const uint8_t* buffer, uint8_t count )
{
    uint8_t cmd, reply, i;

    ENC_nCS_LAT = 0;
    Nop();

    cmd = 0b01111010;
    reply = 0x00;
    ENC_spiXfer( cmd, &reply );
    for ( i = 0; i < count; i++ )
        ENC_spiXfer( buffer[i], &reply );

    ENC_nCS_LAT = 1;
}


void encBitSet( uint8_t r, uint8_t mask )
{
    uint8_t cmd, reply;

    ENC_nCS_LAT = 0;
    Nop();

    cmd = 0b10000000 | ( r & 0b00011111 );
    reply = 0x00;

    ENC_spiXfer( cmd , &reply );
    ENC_spiXfer( mask, &reply );

    ENC_nCS_LAT = 1;
}


void encBitClear( uint8_t r, uint8_t mask )
{
    uint8_t cmd, reply;

    ENC_nCS_LAT = 0;
    Nop();

    cmd = 0b10100000 | ( r & 0b00011111 );
    reply = 0x00;

    ENC_spiXfer( cmd , &reply );
    ENC_spiXfer( mask, &reply );

    ENC_nCS_LAT = 1;
}


void encReset()
{
    uint8_t cmd, reply;

    ENC_nCS_LAT = 0;
    Nop();

    cmd = 0b11111111;
    reply = 0x00;
    ENC_spiXfer( cmd, &reply );

    ENC_nCS_LAT = 1;
}


uint16_t encGetRxBufferFree()
{
    // constants
    const uint16_t uERXST = 0x0000;
    const uint16_t uERXND = 0x18FF;

    uint16_t uERXWRPT = 0, uERXRDPT = 0;
    uint8_t rl = 0, rh = 0;

    while ( 1 ) {
        uint8_t chkcnt = 0, pcnt = 0;
        
        encBankSelect( 1 );
        encReadRegister( R_EPKTCNT, &pcnt );

        encBankSelect( 0 );
        encReadRegister( R_RXWRPTL, &rl );
        encReadRegister( R_RXWRPTH, &rh );
        uERXWRPT = rh * 256U + rl;

        encBankSelect( 1 );
        encReadRegister( R_EPKTCNT, &chkcnt );

        if ( chkcnt == pcnt )
            break;
    }

    encBankSelect( 0 );
    encReadRegister( R_RXRDPTL, &rl );
    encReadRegister( R_RXRDPTH, &rh );
    uERXRDPT = rh * 256U + rl;

    if ( uERXWRPT > uERXRDPT )
        return ( uERXND - uERXST ) - ( uERXWRPT - uERXRDPT );

    if ( uERXWRPT == uERXRDPT )
        return uERXND - uERXST;

    return uERXRDPT - uERXWRPT - 1;
}
