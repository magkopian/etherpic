
//** pxenc.h **//

#ifndef PXENC_H
#define PXENC_H

// Common Registers

// EIR Ethernet Interrupt Request (Flag) Register
#define R_EIR           (0x1C)
#define B_PKTIF         (0x40)
#define B_TXIF          (0x04)

// ESTAT Ethernet Status Register
#define R_ESTAT         (0x1D)
#define B_RXBUSY	(0x04)
#define B_TXABRT	(0x02)
#define B_CLKRDY	(0x01)

// ECON1 Ethernet Control Register 1
#define R_ECON1         (0x1F)
#define B_TXRTS         (0x08)
#define B_RXEN          (0x04)
#define B_BSEL1         (0x02)
#define B_BSEL2         (0x01)

// ECON2 Ethernet Control Register 2
#define R_ECON2         (0x1E)
#define B_AUTOINC	(0x80)
#define B_PKTDEC	(0x40)

// Bank 0 Registers

// Buffer Read Pointer
#define R_ERDPTL	(0x00)
#define R_ERDPTH	(0x01)
// Buffer Write Pointer
#define R_EWRPTL	(0x02)
#define R_EWRPTH	(0x03)
// Transmit Buffer Start - POD 0x0000
#define R_ETXSTL	(0x04)
#define R_ETXSTH	(0x05)
// Transmit Buffer End - POD 0x0000
#define R_ETXNDL	(0x06)
#define R_ETXNDH	(0x07)
// Receive Buffer Start - POD 0x05FA
#define R_ERXSTL	(0x08)
#define R_ERXSTH	(0x09)
// Receive Buffer End - POD 0x1FFF
#define R_ERXNDL	(0x0A)
#define R_ERXNDH	(0x0B)
// RX Read Pointer - POD 0x05FA
#define R_RXRDPTL	(0x0C)
#define R_RXRDPTH	(0x0D)
// RX Write Pointer - POD 0x0000
#define R_RXWRPTL	(0x0E)
#define R_RXWRPTH	(0x0F)

// Bank 1 Registers

// Receive Filter Control Register - POD 1010.0001 (UCEN or CRCEN or BCEN)
#define R_ERXFCON       (0x18)
#define B_UCEN          (0x80)
#define B_MCEN          (0x02)
#define B_BCEN          (0x01)
// Ethernet Packet Count Register
#define R_EPKTCNT       (0x19)

// Bank 2 Registers

// MACON1 MAC Control Register 1
#define R_MACON1	(0x00)
#define B_MARXEN	(0x01)

// MACON3 MAC Control Register 3
#define R_MACON3	(0x02)
#define B_PADCFG2	(0x80)
#define B_PADCFG1	(0x40)
#define B_PADCFG0	(0x20)
#define B_TXCRCEN	(0x10)

// MACON4 MAC Control Register 4
#define R_MACON4	(0x03)
#define B_DEFER         (0x40)

#define R_MABBIPG	(0x04)
#define R_MAIPGL	(0x06)
#define R_MAIPGH	(0x07)

// Maximum frame length POD 0x600 1536
#define R_MAMXFLL	(0x0A)
#define R_MAMXFLH	(0x0B)

// MIIM interface to PHY registers
#define R_MIREGADR	(0x14)
#define R_MIWRL         (0x16)
#define R_MIWRH         (0x17)

// Bank 3 Registers

// MAC Address
#define R_MAADR5        (0x00)
#define R_MAADR6        (0x01)
#define R_MAADR3        (0x02)
#define R_MAADR4        (0x03)
#define R_MAADR1        (0x04)
#define R_MAADR2        (0x05)

// MIIM interface to PHY registers
#define R_MISTAT        (0x0A)
#define B_BUSY          (0x01)

// PHY Registers

#define R_PHCON2	(0x10)
#define B_HDLDIS	(0x01)


void encSetup( uint8_t flags );
void encEnable( void );

void encBankSelect( uint8_t banknum );
void encReadRegister( uint8_t r, uint8_t* val );
void encWriteRegister( uint8_t r, uint8_t val );
void encReadBuffer(  uint8_t* buffer, uint16_t count );
void encWriteBuffer( const uint8_t* buffer, uint8_t count );
void encBitSet( uint8_t r, uint8_t mask );
void encBitClear( uint8_t r, uint8_t mask );
void encReset( void );

uint16_t encGetRxBufferFree( void );

#endif

